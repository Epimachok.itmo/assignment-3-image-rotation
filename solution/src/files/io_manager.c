#include "io_manager.h"

enum command_status open_file(FILE **file,char *file_name, char* mode){
    *file = fopen(file_name, mode);
    if (*file){
        return OK;
    }
    return OPENING_FILE_ERROR;

}
enum command_status close_file(FILE *file){
    if (!fclose(file)){
        return OK;
    }
    return CLOSING_FILE_ERROR;
}

