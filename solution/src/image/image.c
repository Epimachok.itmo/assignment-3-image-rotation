#include "image.h"

struct image create_image(size_t width, size_t height){
    struct image img = {0};
    struct pixel *data = malloc(width*height*sizeof(struct pixel));
    if (data){
        img.height = height;
        img.width = width;
        img.data = data;
    }
    return img;

}

void image_destroy(struct image *img) {
    free((*img).data);
}


