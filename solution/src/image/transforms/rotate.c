#include "rotate.h"

struct image rotate( struct image  img){
    struct image ans = create_image(img.height, img.width);
    for (size_t i = 0; i< ans.height; i++){
        for (size_t j = 0; j<ans.width; j++){
            ans.data[i*ans.width +j] = img.data[ans.height * (ans.width - j - 1) + i];
        }
    }
    return ans;
}
