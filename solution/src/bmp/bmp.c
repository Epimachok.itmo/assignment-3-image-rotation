#include "bmp.h"

uint16_t bmp_padding(size_t width) {
    uint16_t padding_size = (4 - (width * sizeof(struct pixel) % 4)) % 4;
    return padding_size;
}
