#include "bmp_io.h"
#define bf_type 0x4D42
#define bf_reserved 0
#define bi_size 40
#define bi_planes 1
#define bi_bit_count 24
#define bi_compression 0
#define bi_x_pels_per_meter 0
#define bi_y_pels_per_meter 0
#define bi_clr_used 0
#define bi_clr_important 0


enum command_status read_header(FILE *file, struct bmp_header *header){
    if (fread(header, sizeof(struct bmp_header), 1, file) == 1){
        return OK;
    }
    return HEADER_READING_ERROR;
}

enum command_status read_data(FILE *file, struct image *img){
    uint16_t padding_size = bmp_padding((*img).width);
    for (size_t i = 0; i<(*img).height; i++){
        if(fread((*img).data + i*(*img).width, sizeof (struct pixel),
                (*img).width, file) != (*img).width){
            return DATA_READING_ERROR;
        }
        if (fseek(file, padding_size, SEEK_CUR)){
            return DATA_READING_ERROR;
        }
    }
    return OK;
}

enum command_status from_bmp(FILE *file, struct image *img){
    struct bmp_header header = {0};
    if(read_header(file, &header) == OK){
        *img = create_image(header.biWidth, header.biHeight);
        return read_data(file, img);
    }
    return HEADER_READING_ERROR;
}

static struct bmp_header create_header(struct image *img) {
    size_t size_of_image = (sizeof(struct pixel) * (*img).width + bmp_padding((*img).width)) * (*img).height;
    struct bmp_header header = {
            .bfType = bf_type,
            .bfileSize = sizeof(struct bmp_header) + size_of_image,
            .bfReserved = bf_reserved,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = bi_size,
            .biWidth = (*img).width,
            .biHeight = (*img).height,
            .biPlanes = bi_planes,
            .biBitCount = bi_bit_count,
            .biCompression = bi_compression,
            .biSizeImage = size_of_image,
            .biXPelsPerMeter = bi_x_pels_per_meter,
            .biYPelsPerMeter = bi_y_pels_per_meter,
            .biClrUsed = bi_clr_used,
            .biClrImportant = bi_clr_important
    };
    return header;
}

enum command_status write_header(FILE *file, struct bmp_header header){
    if (fwrite(&header, sizeof(struct bmp_header), 1, file)){
        return OK;
    }
    return HEADER_WRITING_ERROR;
}

enum command_status write_data(FILE *file, struct image *img){
    uint16_t padding_size = bmp_padding((*img).width);
    for (size_t i = 0; i<(*img).height; i++){
        if(fwrite((*img).data + i*(*img).width, sizeof (struct pixel),
                 (*img).width, file) != (*img).width){
            return DATA_WRITING_ERROR;
        }
        if ((fseek(file, padding_size, SEEK_CUR))){
            return DATA_WRITING_ERROR;
        }
    }
    return OK;
}

enum command_status to_bmp(FILE *file, struct image *img){
    struct bmp_header header = create_header(img);
    if (write_header(file, header) == OK){
        return write_data(file, img);
    }
    return HEADER_WRITING_ERROR;
}







