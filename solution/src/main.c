#include "bmp_io.h"
#include "io_manager.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "You must input 2 arguments");
        return 1;
    }
    enum command_status status;
    FILE *input_file = NULL;
    FILE *output_file = NULL;
    status = open_file(&input_file, argv[1], "rb");
    if (status != OK){
        return status;
    }
    struct image img = {0};
    status = from_bmp(input_file, &img);
    if (status != OK){
        image_destroy(&img);
        return status;
    }
    status = close_file(input_file);
    if (status != OK){
        image_destroy(&img);
        return status;
    }
    struct image new_img = rotate(img);
    status = open_file(&output_file, argv[2], "wb");
    if (status != OK){
        image_destroy(&img);
        image_destroy(&new_img);
        return status;
    }
    status = to_bmp(output_file, &new_img);
    if (status != OK){
        image_destroy(&img);
        image_destroy(&new_img);
        return status;
    }
    status = close_file(output_file);
    if (status != OK){
        image_destroy(&img);
        image_destroy(&new_img);
        return status;
    }
    image_destroy(&img);
    image_destroy(&new_img);
    return OK;



}
