
#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "stdint.h"
#include "stdlib.h"


struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel *data;
};

struct image create_image(size_t width, size_t height);
void image_destroy(struct image *img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
