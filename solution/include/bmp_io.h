
#ifndef IMAGE_TRANSFORMER_BMP_IO_H
#define IMAGE_TRANSFORMER_BMP_IO_H

#include "bmp.h"
#include "status_codes.h"
#include "stdio.h"

enum command_status from_bmp(FILE *file, struct image *img);
enum command_status to_bmp(FILE *file, struct image *img);

#endif //IMAGE_TRANSFORMER_BMP_IO_H
