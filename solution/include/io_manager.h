#ifndef IMAGE_TRANSFORMER_IO_MANAGER_H
#define IMAGE_TRANSFORMER_IO_MANAGER_H

#include "status_codes.h"
#include "stdio.h"

enum command_status open_file(FILE **file,char *file_name, char* mode);
enum command_status close_file(FILE *file);



#endif //IMAGE_TRANSFORMER_IO_MANAGER_H
